#ifndef MUSICACTION_H
#define MUSICACTION_H

#include <QString>
#include <QSettings>

class MusicAction
{
public:
    enum Action { CLICK, METHOD };

    MusicAction(QSettings &, const QString&);

    Action action;
    QString element;
    QString js;
};

#endif // MUSICACTION_H
