#include "musicaction.h"

MusicAction::MusicAction(QSettings& settings, const QString& name) {
    settings.beginGroup(name);
    QString actionString = settings.value("action").toString();
    if(actionString == QString("click")) {
        action = CLICK;
    } else {
        action = METHOD;
        js = actionString;
    }
    element = settings.value("element").toString();
    settings.endGroup();
}
