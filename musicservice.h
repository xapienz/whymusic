#ifndef MUSICSERVICE_H
#define MUSICSERVICE_H

#include <QUrl>
#include <QSettings>
#include <QMap>
#include <musicaction.h>

class MusicService {
public:
    MusicService();
    MusicService(QSettings &source, const QString &id);
    ~MusicService();
    QString& getName();
    QUrl& getUrl();
    MusicAction& getAction(const QString&);
    bool playing;
private:
    QString name;
    QUrl url;
    QMap<QString, MusicAction> actions;

};

#endif // MUSICSERVICE_H
