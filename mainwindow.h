#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWebKit>
#include <QSettings>
#include <QList>
#include <musicservice.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
public slots:

    void play();
    void back();
    void next();
    void run(const QString& cmd);
    void loadService(int);
    void loadService(const MusicService &);

private:
    Ui::MainWindow *ui;
    void fireClick(const QString&);
    void fireJs(const QString&, const QString&);
    void fireAction(const QString&);
    void init();
    QList <MusicService> services;
    MusicService currentService;
};

#endif // MAINWINDOW_H
