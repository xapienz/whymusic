#include "musicservice.h"

MusicService::MusicService(QSettings& source, const QString& id) {
    source.beginGroup(id);
    name = (source.value("name", "").toString());
    url = QUrl(source.value("url", "").toString());
    MusicAction playAction(source, "play");
    MusicAction pauseAction(source, "pause");
    MusicAction backAction(source, "back");
    MusicAction nextAction(source, "next");
    actions.insert("play", playAction);
    actions.insert("pause", pauseAction);
    actions.insert("back", backAction);
    actions.insert("next", nextAction);
    source.endGroup();
}

MusicService::MusicService() {

}

QString& MusicService::getName() {
    return name;
}

QUrl& MusicService::getUrl() {
    return url;
}

MusicAction& MusicService::getAction(const QString &type) {
    return *actions.find(type);
}

MusicService::~MusicService() {
}
