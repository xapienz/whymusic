#include "mainwindow.h"
#include <qtsingleapplication.h>
#include <QtWebKit>

int main(int argc, char *argv[]) {
    QtSingleApplication app(argc, argv);
    if (app.isRunning()) {
        if(argc>1) {
            app.sendMessage(QString(argv[1]));
        }
        return 0;
    }

    MainWindow w;
    w.show();

    QObject::connect(&app, SIGNAL(messageReceived(const QString&)),
                     &w, SLOT(run(const QString&)));

    return app.exec();
}
