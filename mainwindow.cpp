#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);
    connect(ui->cmbServices, SIGNAL(currentIndexChanged(int)), this, SLOT(loadService(int)));
    ui->playButton->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
    ui->backButton->setIcon(style()->standardIcon(QStyle::SP_MediaSkipBackward));
    ui->nextButton->setIcon(style()->standardIcon(QStyle::SP_MediaSkipForward));
    init();
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::init() {
    QSettings file("services.ini", QSettings::IniFormat);

    QStringList groups = file.childGroups();
    for(int i=0; i<groups.size(); i++) {
        const QString group = groups.at(i);
        MusicService service(file, group);
        services.append(service);
    }

    for(int i=0; i<services.size(); i++) {
        MusicService service = services.at(i);
        ui->cmbServices->addItem(service.getName());
    }

    loadService(0);
}

void MainWindow::loadService(int index) {
    loadService(services.at(index));
}

void MainWindow::loadService(const MusicService& service) {
    currentService = service;
    ui->webView->load(currentService.getUrl());
    currentService.playing = false;
}

void MainWindow::fireAction(const QString & actionString) {

    std::cout << actionString.toStdString() << std::endl;

    MusicAction action = currentService.getAction(actionString);
    switch(action.action) {
    case MusicAction::CLICK:
        fireClick(action.element);
        break;
    case MusicAction::METHOD:
        fireJs(action.element, action.js);
        break;
    }
}

void MainWindow::fireJs(const QString& selector, const QString& js) {
    QWebElement el = ui->webView->page()->mainFrame()->findFirstElement(selector);
    el.evaluateJavaScript(js);
}

void MainWindow::fireClick(const QString &selector) {
    fireJs(selector, "var evObj = document.createEvent('MouseEvents'); evObj.initEvent( 'click', true, true ); this.dispatchEvent(evObj);");
}

void MainWindow::play() {
    run("toggle");
}

void MainWindow::back() {
    run("back");
}

void MainWindow::next() {
    run("next");
}

void MainWindow::run(const QString &cmd) {
    if(cmd=="toggle") {
        if(currentService.playing) {
            fireAction("pause");
            currentService.playing = false;
        } else {
            fireAction("play");
            currentService.playing = true;
        }
    } else {
        fireAction(cmd);
    }
}
