#-------------------------------------------------
#
# Project created by QtCreator 2013-07-25T20:42:05
#
#-------------------------------------------------

QT       += core gui webkit

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = whymusic
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    musicservice.cpp \
    musicaction.cpp

HEADERS  += mainwindow.h \
    musicservice.h \
    musicaction.h

FORMS    += mainwindow.ui

include(./qtsingleapplication/src/qtsingleapplication.pri)
